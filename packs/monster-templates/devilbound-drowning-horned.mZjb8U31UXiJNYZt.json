{
  "_id": "mZjb8U31UXiJNYZt",
  "name": "Devilbound (Drowning, Horned)",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "system": {
    "description": {
      "value": "<p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b> No<br><b>Usable with Summons</b> No<p>A devilbound creature has made a bargain with a devil, promising a service and its soul in exchange for infernal power. The specif ic service depends on the devil’s type and motivations, but always furthers the interests of Hell.<p>“Devilbound creature” is an acquired template that can be added to any creature with 5 or more Hit Dice and Intelligence, Wisdom, and Charisma scores of 3 or higher (referred to hereafter as the base creature). The creature retains all the base creature’s statistics and special abilities except as noted here.\n<p><b>CR:</b> Same as the base creature +1.</p>\n<p><b>Alignment:</b> Any evil. A devilbound creature radiates an evil aura as if it were an evil outsider.</p>\n<p><b>Senses:</b> A devilbound creature gains darkvision 60 ft. and the see in darkness ability.</p>\n<p><b>Armor Class:</b> Natural armor improves by +4.</p>\n<p><b>Defensive Abilities:</b> A devilbound creature gains a +4 bonus on saving throws against poison, resist fire 30, and regeneration 5 (good spells, good weapons).</p>\n<p><b>Weaknesses:</b> The devil-bound creature gains the following weakness.</p>\n<p><em>Contract Bound (Ex): </em>The creature has signed a contract of service in return for this template. The devil must reveal its nature as a creature of Hell when it offers a contract, and it can’t hide the details of the contract in any way. The creature must enter the agreement willingly (without magical compulsion). Usually the creature must perform one or more tasks for the devil, and in exchange the creature gains the template’s abilities, whether immediately, after a specific amount of time, or once the tasks are completed.<p>The contract always includes a clause that damns the creature’s soul to Hell when the creature dies, with credit for the act and possession of the soul going to the devil signing the contract. When the creature dies, its soul is automatically imprisoned in a gem, which immediately appears in Hell as one of the devil’s belongings. If the devil is dead when the creature dies, the creature’s soul is destroyed, and can’t be restored to life except by miracle or wish. If the creature fails to perform the tasks in the allotted time, its soul is still damned and the devil is not obligated to provide the promised abilities.<p>Many contracts state that the devil, its agents, and its allies will not attempt to kill the creature. This doesn’t protect against all devils, but does offer the creature a measure of protection against treachery from the signatory devil.<p>Breaking a contract with a devil is diff icult and dangerous. Furthermore, as long as the contract remains in effect, a slain victim can’t be restored to life after death except by a miracle or wish. If the devilbound creature is restored to life, the devil immediately senses the name and location (as discern location) of the creature responsible.<p><b>Special Attacks:</b> The creature gains the summon universal monster ability and can summon a devil once per day with a 100% chance of success. The devil remains for 1 hour. The creature’s caster level or Hit Dice, whichever is higher, determines the most powerful kind of devil it can summon and the effective spell level of this ability, according to the following table.<br><br></p>\n<table>\n<tbody>\n<tr>\n<td><b>Caster Level</b></td>\n<td><b>Devil</b></td>\n<td><b>Spell Level</b></td>\n</tr>\n<tr>\n<td>3rd</td>\n<td>Lemure</td>\n<td>2nd</td>\n</tr>\n<tr>\n<td>9th</td>\n<td>Bearded devil</td>\n<td>5th</td>\n</tr>\n<tr>\n<td>11th</td>\n<td>Erinyes</td>\n<td>6th</td>\n</tr>\n<tr>\n<td>13th</td>\n<td>Bone devil</td>\n<td>7th</td>\n</tr>\n<tr>\n<td>15th</td>\n<td>Barbed devil</td>\n<td>8th</td>\n</tr>\n<tr>\n<td>17th</td>\n<td>Ice devil</td>\n<td>9th</td>\n</tr>\n</tbody>\n</table>\n<p><br><b>Spell-Like Abilities:</b> The creature gains the following spell-like abilities, depending on the kind of devil it is bound to. The creature uses its Hit Dice or caster level, whichever is higher, as the caster level for its spell-like abilities. Save DCs are based on the creature’s Intelligence, Wisdom, or Charisma, whichever is highest.<p>Accuser: 3/day—clairaudience/clairvoyance, invisibility (self only), summon swarm<br>Barbed: 3/day—hold monster<br>Bearded: 3/day—dimension door, rage<br>Belier: 3/day—charm monster<br>Bone: 3/day—fly, invisibility (self only)<br>Contract: 3/day—bestow curse, detect thoughts, locate creature<br>Drowning: 3/day—hydraulic pushAPG, water breathing<br>Erinyes: 3/day—fear (single target), unholy blight<br>Handmaiden: 3/day—black tentacles; 1/day—true seeing<br>Horned: 3/day—dispel good, fireball<br>Host: 3/day—dimension door, fly<br>Ice: 3/day—cone of cold, ice storm<br>Immolation: 3/day—fire shield, fireball<br>Imp: 3/day—invisibility (self only), polymorph (self only, same size as base creature)<br>Nemesis: 3/day—invisibility, scorching ray; 1/day—blasphemy<br>Pit Fiend: 3/day—quickened fireball, invisibility; 1/day—blasphemy<p><b>Abilities:</b> Adjust the base creature’s ability scores according to the kind of devil it is bound to.<br><br></p>\n<table>\n<tbody>\n<tr>\n<td><b>Devil</b></td>\n<td><b>Str</b></td>\n<td><b>Dex</b></td>\n<td><b>Con</b></td>\n<td><b>Int</b></td>\n<td><b>Wis</b></td>\n<td><b>Cha</b></td>\n</tr>\n<tr>\n<td>Accuser</td>\n<td>—</td>\n<td>2</td>\n<td>2</td>\n<td>—</td>\n<td>2</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Barbed, bearded, host</td>\n<td>2</td>\n<td>2</td>\n<td>2</td>\n<td>—</td>\n<td>—</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Belier</td>\n<td>—</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n<td>2</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Bone, ice</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n<td>2</td>\n<td>2</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Contract, handmaiden</td>\n<td>—</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n<td>2</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Drowning, horned</td>\n<td>2</td>\n<td>2</td>\n<td>—</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Erinyes</td>\n<td>—</td>\n<td>2</td>\n<td>2</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Immolation</td>\n<td>2</td>\n<td>—</td>\n<td>2</td>\n<td>—</td>\n<td>—</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Imp</td>\n<td>—</td>\n<td>2</td>\n<td>—</td>\n<td>2</td>\n<td>—</td>\n<td>2</td>\n</tr>\n<tr>\n<td>Nemesis, pit fiend</td>\n<td colspan=\"6\">+2 to any three different ability scores</td>\n</tr>\n</tbody>\n</table>"
    },
    "changes": [
      {
        "_id": "hicx2k1z",
        "formula": "4",
        "subTarget": "nac",
        "modifier": "untyped"
      },
      {
        "_id": "3x3a8c1e",
        "formula": "2",
        "subTarget": "str",
        "modifier": "untyped"
      },
      {
        "_id": "fxwc3ma3",
        "formula": "2",
        "subTarget": "dex",
        "modifier": "untyped"
      },
      {
        "_id": "6ybnrzhj",
        "formula": "2",
        "subTarget": "cha",
        "modifier": "untyped"
      }
    ],
    "subType": "template",
    "crOffset": "1"
  }
}
