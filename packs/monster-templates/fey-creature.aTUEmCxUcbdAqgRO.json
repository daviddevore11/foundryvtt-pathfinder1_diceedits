{
  "_id": "aTUEmCxUcbdAqgRO",
  "name": "Fey Creature",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "system": {
    "description": {
      "value": "<p><b>Acquired/Inherited Template</b> Both<br><b>Simple Template</b> No<br><b>Usable with Summons</b> No<p>Fey creatures resemble the mundane creatures they derive from, but with brighter colors, delicate features, and elegant wings such as those of a pixie or sprite. Despite their fragile-seeming appearance, fey creatures are every bit as hardy as their non-fey relations, though they sacrifice raw might for grace and the ability to fly. They live long lives, barring death by misadventure, and rarely show outward signs of age.<p>Some fey creatures owe their nature to fey ancestors interbreeding with mortal beings, while others are races in their own right. Still others began life as ordinary creatures and were infused with fey essence through the magic of learned spellcasters or the influence of ancient powers of nature. As a rule, fey creatures rarely dwell in civilized lands, both by preference and because the conditions that give rise to the fey rarely occur in urban surroundings. If not already born into realms of primeval wild or areas touched by great fey powers, fey creatures soon seek them out.<p>Though more prone to mischief than mayhem, fey creatures run the gamut from inimical to sprightly in behavior. Those inclined toward play and jest take a dim view of interlopers lacking in good humor. Such foul-tempered intruders risk humiliation at best if they insult the fey, and much worse if they raise arms against them. More aggressive fey still possess a well-developed though sadistic sense of humor. Such wicked fey use their inborn powers to lure outsiders to their doom, rather than into mere inconvenience.<p>Fey creatures generally have cordial relationships with animals, allies of nature such as druids, and other fey. Exceptions exist where rival communities of fey dwell in proximity to one another. In these cases, any fey creatures in the vicinity ally with one side or the other according to their own inclinations, only rarely standing outside such conflicts. Fey creatures derived from horses and the like often serve as mounts, though only to other fey or to allies of nature who acknowledge them as at least near-equals, if not full partners.<p>“Fey Creature” is an inherited or acquired template that can be added to any living, corporeal creature. A fey creature retains the base creature’s statistics and special abilities except as noted here.<p><b>CR:</b> 9 HD or less, as base creature +1; 10 HD or more, as base creature +2.</p>\n<p><b>Alignment:</b> Any non-lawful.</p>\n<p><b>Type:</b> The creature’s type changes to fey. Do not recalculate HD, BAB, or saves.</p>\n<p><b>Senses:</b> A fey creature gains low-light vision.</p>\n<p><b>Armor Class:</b> Reduce the creature’s natural armor, if any, by 1 (minimum of 0).</p>\n<p><b>Defensive Abilities:</b> A fey creature gains a +4 bonus on saves against mind-affecting effects, resist cold and electricity 10, and DR 5/cold iron (if 11 HD or less) or DR 10/cold iron (if 12 HD or more).</p>\n<p><b>Speed:</b> Unless the base creature flies better, the fey creature flies at 1-1/2 times the base creature’s land speed (good maneuverability), rounded down to the nearest multiple of 5 feet. If the creature already has flight with a maneuverability of good, it increases to perfect.</p>\n<p><b>Special Abilities:</b> A fey creature gains one of the following abilities for every 4 HD or fraction thereof.</p>\n<p><em>Camouflage (Ex) </em>A fey creature can use Stealth to hide in any sort of natural terrain, even if the terrain does not grant cover or concealment. It gains a +4 racial bonus on Stealth checks. This bonus does not stack with any racial Stealth bonus possessed by the base creature.</p>\n<p><em>Change Shape (Su) </em>A fey creature can change shape into a single form. Possible forms include a normal specimen of its base creature, a humanoid creature within one size category, or an animal within one size category. In all cases, the fey creature appears as the same individual of its alternate form each time it changes shape. The type of polymorph spell used should be chosen as appropriate based on the alternate form, such as alter self for taking humanoid form. This ability can be selected more than once, granting an additional form each time.</p>\n<p><em>Energy Resistance (Ex) </em>A fey creature gains resistance 10 to one energy type, or increases an existing resistance by 10. Resistance increased beyond 30 becomes immunity instead. This ability can be selected more than once.</p>\n<p><em>Evasion (Ex) </em>A fey creature gains evasion, as the rogue ability of the same name.</p>\n<p><em>Long Step (Su) </em>A fey creature can teleport up to 10 feet per Hit Die as a move action. It may use this ability once every 1d4 rounds.</p>\n<p><em>Spell Resistance (Ex) </em>A fey creature gains SR equal to 11 + its CR. This does not stack with any existing SR possessed by the base creature.</p>\n<p><em>Trackless Step (Ex) </em>A fey creature does not leave a trail in natural surroundings and cannot be tracked. It can choose to leave a trail, if it so desires.</p>\n<p><em>Vanish (Su) </em>As a swift action, a fey creature can vanish for 1 round as if affected by invisibility. It can use this ability for 1 round per day per Hit Die.</p>\n<p><em>Woodland Stride (Ex) </em>A fey creature can move through any sort of undergrowth (such as natural thorns, briars, overgrown areas, and similar terrain) at its normal speed and without taking damage or suffering any other impairment. Thorns, briars, and overgrown areas that have been magically manipulated to impede motion still affect it. Optionally, this ability may function in a different type of terrain, to allow the fey creature to move through, swamps, rocky areas, ice, and so forth. Whatever the choice, this ability only functions in one type of terrain. This ability can be selected more than once, for a different terrain each time.</p>\n<p><b>Spell-Like Abilities:</b> A fey creature with an Intelligence or Wisdom score of 8 or more has a cumulative number of spell-like abilities depending on its Hit Dice. Unless otherwise noted, an ability is usable once per day. Caster level equals the creature’s HD (or the caster level of the base creature’s spell-like abilities, whichever is higher).</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td>1–2</td>\n<td><em>Dancing lights </em>3/day, <em>faerie fire</em></td>\n</tr>\n<tr>\n<td>3–4</td>\n<td><em>Entangle</em>, <em>glitterdust</em></td>\n</tr>\n<tr>\n<td>5–6</td>\n<td><em>Deep slumber</em></td>\n</tr>\n<tr>\n<td>7–8</td>\n<td><em>Major image</em></td>\n</tr>\n<tr>\n<td>9–10</td>\n<td><em>Confusion</em></td>\n</tr>\n<tr>\n<td>11–12</td>\n<td><em>Feeblemind</em></td>\n</tr>\n<tr>\n<td>13–14</td>\n<td><em>Mislead</em></td>\n</tr>\n<tr>\n<td>15–16</td>\n<td><em>Project image</em></td>\n</tr>\n<tr>\n<td>17–18</td>\n<td><em>Irresistible dance</em></td>\n</tr>\n<tr>\n<td>19–20</td>\n<td><em>Scintillating pattern</em></td>\n</tr>\n</tbody>\n</table>\n<hr>\n<p><br><b>Abilities:</b> A fey creature gains a +4 bonus to Dexterity and a +2 bonus to Intelligence and Charisma. A fey creature receives a –2 penalty to Strength. Fey creatures derived from creatures without an Intelligence score gain an Intelligence of 3.</p>\n<p><b>Skills:</b> A fey creature with racial Hit Dice has skill points per racial Hit Die equal to 6 + its Intelligence modifier. It gains Acrobatics, Bluff, Fly, and Stealth as class skills.</p>\n<p><b>Languages:</b> Fey creatures speak Sylvan as well as any languages spoken by the base creature.</p>"
    },
    "changes": [
      {
        "_id": "52jqw8b9",
        "formula": "(@ac.natural.total >= 1) ? -1 : 0",
        "subTarget": "nac",
        "modifier": "penalty"
      },
      {
        "_id": "49zh108t",
        "formula": "4",
        "subTarget": "dex",
        "modifier": "untyped"
      },
      {
        "_id": "3ioq7jh6",
        "formula": "2",
        "subTarget": "int",
        "modifier": "untyped"
      },
      {
        "_id": "wte49dpe",
        "formula": "2",
        "subTarget": "cha",
        "modifier": "untyped"
      },
      {
        "_id": "84rjwfkb",
        "formula": "-2",
        "subTarget": "str",
        "modifier": "untyped"
      }
    ],
    "subType": "template",
    "crOffset": "@attributes.hd.total >= 10 ? 2 : 1"
  }
}
