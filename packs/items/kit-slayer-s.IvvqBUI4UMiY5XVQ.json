{
  "_id": "IvvqBUI4UMiY5XVQ",
  "name": "Kit, Slayer's",
  "type": "container",
  "img": "icons/containers/chest/chest-reinforced-steel-red.webp",
  "system": {
    "inventoryItems": [
      {
        "name": "Backpack",
        "type": "container",
        "img": "systems/pf1/icons/items/inventory/backpack.jpg",
        "system": {
          "description": {
            "value": "<p>This leather knapsack has one large pocket that closes with a buckled strap and holds about 2 cubic feet of material. Some may have one or more smaller pockets on the sides.</p>"
          },
          "weight": {
            "value": 2
          },
          "price": 2
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.lSCPUK5Ea6R0t4fz"
          }
        },
        "_id": "kX7J5FDSzOypqQnO"
      },
      {
        "name": "Pouch, Belt",
        "type": "loot",
        "img": "icons/containers/bags/coinpouch-simple-leather-tan.webp",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>A belt pouch is crafted of soft cloth or leather. They typically hold up to 10 lb. or 1/5 cubic ft. of items.</p>\n<p><b>Empty Weight</b>: 1/2 lb.<sup>1</sup> <b>Capacity</b>: 1/5 cubic ft./10 lb.<sup>1</sup></p>\n<p class=\"source\"><sup>1</sup> When made for Medium characters. Weighs one-quarter the normal amount when made for Small characters. Weighs twice the normal amount when made for Large characters. Containers carry one-quarter the normal amount when made for Small characters.</p>"
          },
          "weight": {
            "value": 0.5
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.wjvxmolafiegvesl"
          }
        },
        "_id": "KeYvwBXQwqHFCSiy"
      },
      {
        "name": "Flint And Steel",
        "type": "loot",
        "img": "icons/commodities/stone/rock-chunk-grey.webp",
        "system": {
          "description": {
            "value": "<p>Lighting a <em>torch</em> with a <em>flint and steel</em> is a full-round action. Lighting any other fire with them takes at least that long.</p>"
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.ygmrcisvftnvjyob"
          }
        },
        "_id": "fNJZEVSUmZDWBG5H"
      },
      {
        "name": "Soap",
        "type": "consumable",
        "img": "systems/pf1/icons/items/inventory/soap.jpg",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PPZO9410</em></p>\n<p>You can use this thick block of soap to scrub clothes, pots, linens, or anything else that might be dirty. A bar of soap has approximately 50 uses.</p>"
          },
          "weight": {
            "value": 0.5
          },
          "price": 0.01,
          "actions": [
            {
              "_id": "ootoocddr3l2eoat",
              "name": "Use",
              "img": "systems/pf1/icons/items/inventory/soap.jpg",
              "activation": {
                "type": "special",
                "unchained": {
                  "type": "special"
                }
              },
              "actionType": ""
            }
          ],
          "uses": {
            "per": "charges",
            "value": 50,
            "maxFormula": "50"
          },
          "tag": "soap",
          "subType": "misc"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.yMoclCylbCRKyaZF"
          }
        },
        "_id": "PkNVv0cflnHS5AKC"
      },
      {
        "name": "Torch",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/torch.jpg",
        "system": {
          "description": {
            "value": "<p><b>Price</b> 1 cp; <b>Weight</b> 1 lb.</p>\n<p>A torch burns for 1 hour, shedding normal light in a 20-foot radius and increasing the light level by one step for an additional 20 feet beyond that area (darkness becomes dim light and dim light becomes normal light). A torch does not increase the light level in normal light or bright light. If a torch is used in combat, treat it as a one-handed improvised weapon that deals bludgeoning damage equal to that of a gauntlet of its size, plus 1 point of fire damage.</p>"
          },
          "weight": {
            "value": 1
          },
          "price": 0.01,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.snfogneawzopfzzl"
          }
        },
        "_id": "EliU1IoofO1QWWTx"
      },
      {
        "name": "Waterskin",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/waterskin.jpg",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>A water or wineskin holds 1/2 gallon of liquid and weighs 4 lb when full.</p>\n<p><b>Empty Weight</b>: -; <b>Capacity</b>: 1/2 gallon/4 lb.<sup>1</sup></p>\n<p class=\"source\"><sup>1</sup> When made for Medium characters. Weighs one-quarter the normal amount when made for Small characters. Weighs twice the normal amount when made for Large characters. Containers carry one-quarter the normal amount when made for Small characters.</p>\n<h3>Magic Containers</h3>\n<p>Magic containers come in almost as many varieties as there are <em>alchemists</em>, and so are of inestimable value to <em>alchemists</em> with specific needs. Some of the most well-known magic containers are described here.</p>\n<p>Because of a varied nature of alchemical items and their effects, not all combinations of alchemical items and magic bottles are viable or even make sense, even though they technically might be allowed by the rules. The GM should have the final say on whether or not a particular alchemical item can function within one of the magical containers listed in this section.</p>\n<table>\n<thead>\n<tr>\n<th colspan=\"4\">Magic Containers</th>\n</tr>\n<tr>\n<th>Item</th>\n<th>Price</th>\n<th>Weight</th>\n<th>Source</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td><i>Focusing Flask</i></td>\n<td>700 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Retort of Control</i></td>\n<td>13,000 gp</td>\n<td>-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Vial of Efficacious Medicine</i></td>\n<td>7,000 gp</td>\n<td>-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Winged Bottle</i></td>\n<td>1,620 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n</tbody>\n</table>"
          },
          "weight": {
            "value": 4
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.bsztkhkrhgmsbqpt"
          }
        },
        "_id": "NS5zc8rORSztiMy9"
      },
      {
        "name": "Rations, Trail",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/meat.jpg",
        "system": {
          "description": {
            "value": "<h4>Rations, Trail</h4>\n<p><b>Price</b> 5 sp; <b>Weight</b> 1 lb.</p>\n<p>The listed price is for a day's worth of food. This bland food is usually some kind of hard tack, jerky, and dried fruit, though the contents vary from region to region and the race of those creating it. As long as it stays dry, it can go for months without spoiling.</p>"
          },
          "weight": {
            "value": 1
          },
          "price": 0.5,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.manvtbznrwjmnrua"
          }
        },
        "_id": "Poqxdf31sseFJu2q"
      },
      {
        "name": "Rope",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "description": {
            "value": "<h4>Rope</h4>\n<p><b>Hempen</b> The DC to escape hemp rope bonds is equal to 20 + the CMB of the creature that tied the bonds. Ropes do not need to make a check every round to maintain the pin. If the DC to escape is higher than 20 + the tying creatures CMB, the tied up creature cannot escape from the bonds, even with a natural 20 on the check. This rope has 2 hit points and can be burst with a DC 23 Strength check. <b>Price</b> 1 gp; <b>Weight</b> 10 lbs.</p>\n<p><b>FYI</b>: The <em>Equipment Trick</em> feat provides a number of options for using this item in combat.</p>\n<p><b>Silk Rope</b> This 50-foot length of silk rope has 4 hit points and can be broken with a DC 24 Strength check. <b>Price</b> 10 gp; <b>Weight</b> 5 lbs.</p>\n<p><b>Bloodvine Rope</b> This 50-foot length of tough, lightweight rope is made from alchemically treated bloodvine, a rare scarlet-colored vine that grows only in warm jungle environments. Though prized by climbers for its durability, bloodvine can also be used to bind creatures. Bloodvine rope has a <em>hardness</em> of 5 and 10 <em>hit points</em>, and can be broken with a DC 30 <em>Strength</em> check. A creature bound by bloodvine rope can escape with a DC 35 <em>Escape Artist</em> check or a DC 30 <em>Strength</em> check. The DC to create bloodvine rope with <em>Craft</em> (alchemy) is 30. <b>Price</b> 200 gp; <b>Weight</b> 5 lbs. <b>Source</b> <em>PRG:ACG</em></p>\n<p><b>Spider's Silk Rope</b> This 50-foot length of rope is woven of strands of silk from monstrous spiders. Rare to virtually nonexistent on the surface world, it is commonly used by the dark elves, though shorter spider's silk rope scraps (generally no more than 10 feet long) occasionally appear among goblins. Spider's silk rope has 6 hit points and can be broken with a DC 25 Strength check. <b>Price</b> 100 gp; <b>Weight</b> 4 lbs.</p>"
          },
          "weight": {
            "value": 10
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.qlmtnppwxkubqyhp"
          }
        },
        "_id": "PzEXOry2T3oRWhxV"
      },
      {
        "name": "Bedroll",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/cloth-blue.jpg",
        "system": {
          "description": {
            "value": "<p>This consists of two woolen sheets sewn together along the bottom and one side to create a bag for sleeping in. Some have cloth straps along the open side so the bedroll can be tied closed while you are sleeping. It can be rolled and tied into a tight coil for storage or transport. Most people use a <em>blanket</em> with the bedroll to stay warm or provide a ground cushion.</p>"
          },
          "weight": {
            "value": 5
          },
          "price": 0.1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.iegwwsarycqwoezj"
          }
        },
        "_id": "W2RLoLNXP1KpM3ya"
      },
      {
        "name": "Pot, Cooking (Iron)",
        "type": "loot",
        "img": "icons/tools/cooking/cauldron-empty.webp",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>Cooking pots come in a variety of materials, but the most common is formed of iron.</p>\n<p>A mithral cooking pot weighs 2 lbs. and costs 2,001 gp.</p>\n<p><b>Empty Weight</b>: 2 lb.; <b>Capacity</b>: 1 gallon/8 lb.</p>"
          },
          "weight": {
            "value": 4
          },
          "price": 0.8,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.habofoapdifirevm"
          }
        },
        "_id": "M8RPUWZgIZOG7zV3"
      },
      {
        "name": "Mess Kit",
        "type": "container",
        "img": "icons/containers/chest/chest-simple-box-red.webp",
        "system": {
          "description": {
            "value": "<p>This kit includes a plate, bowl, cup, fork, knife, and spoon, made of wood, horn, or tin. Each item has a handle or small hole, and can be tied together using the included leather cord.</p>"
          },
          "inventoryItems": [
            {
              "name": "Plate",
              "type": "loot",
              "img": "systems/pf1/icons/items/inventory/dice.jpg",
              "system": {
                "tags": ["Mundane", "Cooking"],
                "weight": {
                  "value": 0.4
                },
                "equipped": false,
                "subType": "tradeGoods"
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.tVkpdPfIJomoFjUl"
                }
              },
              "_id": "HrXDTu0yJArUbpYe"
            },
            {
              "name": "Bowl",
              "type": "loot",
              "img": "icons/containers/kitchenware/bowl-clay-brown.webp",
              "system": {
                "tags": ["Mundane", "Cooking"],
                "weight": {
                  "value": 0.2
                },
                "equipped": false,
                "subType": "tradeGoods"
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.fASUzJozDXZldwNT"
                }
              },
              "_id": "B5gFNbiUqEKlkI3d"
            },
            {
              "name": "Fork",
              "type": "loot",
              "img": "icons/tools/cooking/fork-steel-tan.webp",
              "system": {
                "tags": ["Mundane", "Cooking"],
                "weight": {
                  "value": 0.1
                },
                "equipped": false,
                "subType": "gear"
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.jzu5QqK20bW9gkN1"
                }
              },
              "_id": "75V6g53wDWtG50uT"
            },
            {
              "name": "Knife",
              "type": "loot",
              "img": "icons/tools/cooking/knife-chef-steel-brown.webp",
              "system": {
                "tags": ["Mundane", "Cooking", "Crafting", "Survival"],
                "weight": {
                  "value": 0.1
                },
                "equipped": false,
                "subType": "gear"
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.GZtAdQ7gJlCe9z64"
                }
              },
              "_id": "G0t7OoDwQXQgSuli"
            },
            {
              "name": "Spoon",
              "type": "loot",
              "img": "icons/tools/cooking/soup-ladle.webp",
              "system": {
                "tags": ["Mundane", "Cooking"],
                "weight": {
                  "value": 0.1
                },
                "equipped": false,
                "subType": "gear"
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.Asg5Ek4CFqb80P7C"
                }
              },
              "_id": "wPKymHrQQpgJ2oYO"
            },
            {
              "name": "Cup",
              "type": "loot",
              "img": "systems/pf1/icons/items/inventory/dice.jpg",
              "system": {
                "tags": ["Mundane", "Cooking"],
                "weight": {
                  "value": 0.1
                },
                "subType": "tradeGoods",
                "equipped": false
              },
              "flags": {
                "core": {
                  "sourceId": "Compendium.pf1.items.QqdYeaj0M8e5FWWK"
                }
              },
              "_id": "ssTkq1uXfAO6KeFH"
            }
          ]
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.Ueae6c2qqJJGpncA"
          }
        },
        "_id": "W7nYepF0QTesg5yF"
      },
      {
        "name": "Manacles, Common",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b>: <em>PZO1110</em></p>\n<p>Manacles can bind a Medium creature. A manacled creature can use the <em>Escape Artist</em> skill to slip free (DC 30, or DC 35 for masterwork manacles). Breaking the manacles requires a <em>Strength</em> check (DC 26, or DC 28 for masterwork manacles). Manacles have <em>hardness</em> 10 and 10 <em>hit points</em>.</p>\n<p>Most manacles have locks; add the cost of the lock you want to the cost of the manacles.</p>\n<p>For the same cost, you can buy manacles for a Small creature. For a Large creature, manacles cost 10 times the indicated amount, and for a Huge creature, 100 times the indicated amount. Gargantuan, Colossal, Tiny, Diminutive, and Fine creatures can be held only by specially made manacles, which cost at least 100 times the indicated amount.</p>\n<p><b>Expanded Information</b></p>\n<p class=\"source\"><b>Source</b>: <em>PZO9293</em></p>\n<p><b>Fetters</b>: These manacles are specifically designed to fit around the ankles, and use the same rules for breaking, escape, and cost relative to size. A creature in fetters is entangled and can move at only half speed. In addition, a fettered creature must succeed at a DC 15 <em>Acrobatics</em> check to move more than its (reduced) speed in a round. If it fails the check by 5 or more, the creature falls prone.</p>\n<p><b>Frontal Restraint</b>: A creature whose wrists are bound with manacles can wield only one melee weapon or shield in combat (the weapon may be a two-handed weapon). It cannot make use of ranged weapons, except for crossbows (but even these cannot be reloaded by the bound creature). Any attack the creature makes while manacled takes a -4 penalty. The creature can attempt skill checks using its hands, but at a -5 penalty (-15 on Disable Device checks to pick the lock of manacles the creature is wearing).</p>\n<p><b>Rear Restraint</b>: Manacles might be employed to keep a creature's hands behind its back. In such cases, the creature cannot use its hands to effectively employ any weapon or shield. The creature can attempt to use its hands to perform a skill, but at a -10 penalty (-25 on Disable Device checks to pick the lock of manacles the creature is wearing). While manacled, a creature can attempt to flip its arms beneath its legs, bringing its hands in front of itself. This requires a successful DC 25 Escape Artist check (a separate Escape Artist check is then required to escape the manacles).</p>\n<p><b>Restraining Two Individuals</b>: Two creatures might share one set of manacles. While manacled together, the manacled individuals must remain adjacent to one another. If they are not working together, they are both treated as grappled. If they are working together, they can move together in the same round (using the movement speed of the slower creature). A manacled creature can forfeit its turn during combat to mimic the other manacled creature's movements, allowing that creature to act as if unrestrained.</p>"
          },
          "weight": {
            "value": 2
          },
          "price": 15,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.zompehbhvegvncwu"
          }
        },
        "_id": "a3mnlQpJTVCMWuAq"
      }
    ]
  }
}
