{
  "_id": "nEltI9y37rKJTeWo",
  "name": "Dwarf",
  "type": "race",
  "img": "systems/pf1/icons/races/dwarf.png",
  "system": {
    "description": {
      "value": "<h2>Starting Age</h2><table><thead><tr><th>Adulthood</th><th>Intuitive<sup>1</sup></th><th>Self-Taught<sup>2</sup></th><th>Trained<sup>3</sup></th></tr></thead><tbody><tr><td>40 years</td><td><p>+3d6 years<br>(43 – 58 years)</p></td><td><p>+5d6 years<br>(45 – 70 years)</p></td><td><p>+7d6 years<br>(47 – 82 years)</p></td></tr></tbody></table><p><sup>1</sup> This category includes barbarians, oracles, rogues, and sorcerers.<br><sup>2</sup> This category includes bards, cavaliers, fighters, gunslingers, paladins, rangers, summoners, and witches.<br><sup>3</sup> This category includes alchemists, clerics, druids, inquisitors, magi, monks, and wizards.</p><h2>Height and Weight</h2><table><thead><tr><th>Gender</th><th>Base Height</th><th>Height Modifier</th><th>Base Weight</th><th>Weight Modifier</th></tr></thead><tbody><tr><td>Male</td><td>3 ft. 9 in.</td><td><p>+2d4 in.<br>(3 ft. 11 in. – 4 ft. 5 in.)</p></td><td>150 lbs.</td><td><p>+(2d4×7 lbs.)<br>(164 – 206 lbs.)</p></td></tr><tr><td>Female</td><td>3 ft. 7 in.</td><td><p>+2d4 in.<br>(3 ft. 9 in. – 4 ft. 3 in.)</p></td><td>120 lbs.</td><td><p>+(2d4×7 lbs.)<br>(134 – 176 lbs.)</p></td></tr></tbody></table><h2>Standard Racial Traits</h2><ul><li><strong>Ability Score Modifiers</strong>: Dwarves are both tough and wise, but also a bit gruff. They gain +2 <em>Constitution</em>, +2 <em>Wisdom</em>, and –2 <em>Charisma</em>.</li><li><strong>Type</strong>: Dwarves are <em>humanoids</em> with the <em>dwarf</em> subtype.</li><li><strong>Size</strong>: Dwarves are Medium creatures and thus receive no bonuses or penalties due to their size.</li><li><strong>Base Speed</strong>: (Slow and Steady) Dwarves have a base speed of 20 feet, but their speed is never modified by armor or encumbrance.</li><li><strong>Languages</strong>: Dwarves begin play speaking Common and Dwarven. Dwarves with high <em>Intelligence</em> scores can choose from the following: Giant, Gnome, Goblin, Orc, Terran, and Undercommon.</li></ul><h3>Defense Racial Traits</h3><ul><li><strong>Defensive Training</strong>: Dwarves gain a +4 <em>dodge bonus</em> to AC against monsters of the <em>giant</em> subtype.</li><li><strong>Hardy</strong>: Dwarves gain a +2 <em>racial bonus</em> on saving throws against <em>poison</em>, spells, and <em>spell-like abilities</em>.</li><li><strong>Stability</strong>: Dwarves gain a +4 <em>racial bonus</em> to their <em>Combat Maneuver Defense</em> when resisting a <em>bull rush</em> or <em>trip</em> attempt while standing on the ground.</li></ul><h3>Feat and Skill Racial Traits</h3><ul><li><strong>Greed</strong>: Dwarves gain a +2 <em>racial bonus</em> on @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.QGxoGsSIAOoe5dTW]{Appraise} checks made to determine the price of non-magical goods that contain precious metals or gemstones.</li><li><strong>Stonecunning</strong>: Dwarves gain a +2 bonus on @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.2h6hz5AkTDxKPFxr]{Perception} checks to notice unusual stonework, such as traps and hidden doors located in stone walls or floors. They receive a check to notice such features whenever they pass within 10 feet of them, whether or not they are actively looking.</li></ul><h3>Senses Racial Traits</h3><ul><li><strong>Darkvision</strong>: Dwarves can see perfectly in the dark up to 60 feet.</li></ul><h3>Offensive Racial Traits</h3><ul><li><strong>Hatred</strong>: Dwarves gain a +1 <em>racial bonus</em> on <em>attack rolls</em> against <em>humanoid</em> creatures of the <em>orc</em> and <em>goblinoid</em> subtypes because of their special training against these hated foes.</li><li><strong>Weapon Familiarity</strong>: Dwarves are proficient with battleaxes, heavy picks, and warhammers, and treat any weapon with the word “dwarven” in its name as a martial weapon.</li></ul>"
    },
    "tags": ["Core", "Northern (Crown of the World)"],
    "changes": [
      {
        "_id": "dvv3nq7b",
        "formula": "2",
        "subTarget": "con",
        "modifier": "racial"
      },
      {
        "_id": "8kfc2w3s",
        "formula": "2",
        "subTarget": "wis",
        "modifier": "racial"
      },
      {
        "_id": "zzu7hohk",
        "formula": "-2",
        "subTarget": "cha",
        "modifier": "racial"
      }
    ],
    "changeFlags": {
      "noEncumbrance": true,
      "mediumArmorFullSpeed": true,
      "heavyArmorFullSpeed": true
    },
    "contextNotes": [
      {
        "formula": "",
        "operator": "add",
        "target": "misc",
        "subTarget": "ac",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[4]] Dodge vs Giants"
      },
      {
        "formula": "",
        "operator": "add",
        "target": "savingThrows",
        "subTarget": "allSavingThrows",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[2]] Racial vs Poisons, Spells and Spell-likes"
      },
      {
        "formula": "",
        "operator": "add",
        "target": "misc",
        "subTarget": "cmd",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[4]] Racial vs Bull Rush and Trip while on ground"
      },
      {
        "formula": "",
        "operator": "add",
        "target": "skill",
        "subTarget": "skill.apr",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[2]] Racial to Appraise Items with Gems"
      },
      {
        "formula": "",
        "operator": "add",
        "target": "skill",
        "subTarget": "skill.per",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[2]] to Notice Unusual Stonework"
      },
      {
        "formula": "",
        "operator": "add",
        "target": "attacks",
        "subTarget": "attack",
        "modifier": "",
        "priority": 0,
        "value": 0,
        "text": "+[[1]] Racial vs Humanoids (Orc, Goblinoid)"
      }
    ],
    "languages": {
      "value": ["common", "dwarven"]
    },
    "subTypes": ["Dwarf"]
  }
}
